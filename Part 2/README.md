### Part 2

The second part of this project extends the first part. We now assume that:

1. The number of clients is different - tendentially superior - to the number of servers.
2. KOS is persistent, meaning that the data stored in it survive the activities that created it or accessed it.

In the first part there was a biunivocal correspondence between a client and its server. Now, a client puts a request in the buffer which will be picked up by an available server.

#### Parallelism in the access to KOS

In this part we increase the concurrency in the access to KOS data with the following options:

1. Allow parallel access to different partitions (already implemented in part 1).
2. Allow read access in parallel for a partition.
3. Allow access in parallel to distinct lists in a partition.
4. Allow for search sequences (iterate over a list before reaching the place to *put* or *remove*) and parallel read accesses in a partition.

#### KOS' data persistency

Each partition is stored in a file named `fshardId`. The file is updated as the partition in memory is changed. The flow of data between KOS' memory and the file system obeys the following rules:

- The initialization of each partition in memory is done from the corresponding file in the system's initialization phase.
    - If the file does not exist we assume the partition has no data, so it is initialized empty in memory and the corresponding file is created.
- Read operations do not alter the partition in memory, therefore do not imply any alteration in the partition-file.
- Write is atomic, that is, each write operation resulting from a `put` or `remove` request is done in the partition in memory and is immediatelly propagated to the partition-file.
    - An operation that implies a write (*put*, *remove*) is only considered concluded after the data is written in the memory and in the file. We consider that the data was written to the file once the call to the function of the UNIX file system API, that performs the write operation, returns.

The data to store in the partition file has the following structure:

- Number of key-value pairs stored in the file - `int NPKV` - followed by the sequence of key-value pairs without any particular order.

This structure, although simple, requires searching for the key-value pairs that we intend to access - to delete (*remove*) or update (*put*) - in a sequential scan of the file. The following optimizations are implemented to avoid the sequential scanning of the files and keep them compact.

1. Store in volatile memory the offset of the key-value in the file, thus avoiding the necessity to scan the file.
2. Periodically compact the file, removing empty registries resulting from *remove* operations.
3. Reoccupy empty key-value slots on new insertions by keeping their offsets in memory.
