/*
 * list.c - implementation of the KV_t list functions 
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "list.h"
#include "kos_client.h"



list_t* lst_new()
{
   list_t *list;
   list = (list_t*) malloc(sizeof(list_t));
   list->first = NULL;
   return list;
}


void lst_destroy(list_t *list)
{
   lst_iitem_t *aux, *aux2;
   aux = list->first;
   if(aux == NULL)
      return;
   list->first = NULL;
   aux2 = aux->next;
   while(aux!=NULL){
      free(aux);
      aux = aux2;
      if (aux != NULL)
         aux2 = aux->next;
   }
}


void lst_insert(list_t *list, char *key, char *value)
{
   lst_iitem_t *item, *aux;

   aux=list->first;

   item = (lst_iitem_t *) malloc(sizeof(lst_iitem_t));
   
   strcpy(item->KV.key, key);
   strcpy(item->KV.value, value);
   
   
   if(aux == NULL){
	  item->next = NULL;
      list->first = item;
  }
   else{
      item->next = list->first;
      list->first = item;
   }
}


int lst_remove(list_t *list, char *key)
{  
   lst_iitem_t *aux, *aux2;
   aux = list->first;
   if(aux == NULL)
      return -1;
   if(!strcmp(aux->KV.key, key)){
      list->first = aux->next;
      free(aux);
      return 0;
   }
   aux2 = aux;
   aux = aux->next;
   do{
      if(aux != NULL && !strcmp(aux->KV.key, key)){
         aux2->next = aux->next;
         free(aux);
         return 0;
      }
      else if (aux == NULL) return -1;
      aux2 = aux;
      aux = aux->next;
   } while(aux != NULL);
   return -1;
}


void lst_print(list_t *list)
{
   lst_iitem_t *aux = list->first;
   if(aux == NULL)
      return;
   while(aux->next != NULL){
	  printf("%s,%s\n", aux->KV.key, aux->KV.value);
      aux = aux->next;
   }
   printf("%s,%s\n", aux->KV.key, aux->KV.value);
}


char *lst_search(list_t *list, char *key){
	
	lst_iitem_t *aux = list->first;
	char *value = NULL;
	
	while(aux != NULL){
		if(!strcmp(aux->KV.key, key)){
			value = (char *) malloc (sizeof(char)*strlen(aux->KV.value)+1);
			strcpy(value, aux->KV.value);
			break;
		}
		aux = aux->next;
	}
	
	return value;
}


