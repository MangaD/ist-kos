#include <kos_client.h>
#include <stdlib.h>
#include <stdio.h>
//
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
//
#include <errno.h>//Imprimir mensagens de erro
#include <unistd.h>//Para usar o close(fd) por exemplo
#include <sys/types.h>//Para usar o open
#include <sys/stat.h>//Para o open
#include <fcntl.h>//Para o open
//
#include "delay.h"
#include "hash.h"
#include "list.h"
#include "listInts.h"

#define TRUE 1
#define KEY_SIZE 20

/* Shard Structure */
typedef struct shard {
	int shardId;
	list_t *KV[HT_SIZE];
} shard;
//
/* Buffer Structure */
typedef struct buf{
	char command[20];
	int shardId;
	KV_t *KV;
	int sizeKVArray;
	sem_t bufferSemaphore;//Sincroniza o servidor com o seu cliente.
} buf;

//Buffer array
buf *buffer;
//Shard arrays
shard *shards;

int buffer_size;
int numShards;

//Para as threads servidoras:
int *serverIds;
pthread_t *threads;
//

//Mecanismos de paralelismo:
sem_t clientMayAsk, serverMayProvide;//Tipo produtor/consumidor.
//sem_init(&clientMayAsk, 0, buffer_size);//No início os clients podem fazer buffer_size pedidos.
//sem_init(&serverMayProvide, 0, 0);//No início, como ainda não existem pedidos, os servidores não podem responder a pedidos.

pthread_mutex_t clientMutex;//Usado para impedir mais do que um cliente de inserir um pedido na mesma posição do buffer.
pthread_mutex_t serverMutex;//Usado para impedir mais do que um servidor de responder ao mesmo cliente.
pthread_mutex_t *shardAndListMutex;//Usado para impedir o accesso de escrita à mesma lista de um shard por servidores diferentes.
pthread_mutex_t *fileMutex;
pthread_mutex_t *bufferMutex;//Usado para impedir que mais de um cliente use a mesma posição do buffer 
//(por exemplo, num buffer de tamanho 5, o cliente da posição 4 acaba de usar o buffer, pode então vir um novo cliente fazer um pedido, mas a posição 0 ainda pode estar em uso...)
//

//Posições do buffer em que os novos clientes/servidores podem trabalhar.
int servptr=0;//Posição onde pode vir o próximo servidor.
int cliptr=0;//Posição onde pode vir o próximo cliente.
//
char sentinel = '<';//Usado para remover KV's do ficheiro
char endChar = '\n';

listInt_t **offsets;//Usado para saber as posições de cada ficheiro em que se removeram KV's (para nas futuras inserções usar essas posições, evitando assim o varrimento)

/* Elimina do ficheiro os slots de KV's eliminados, isto é, linhas que começem por '<'. Função chamada apenas no kos_init.*/
void eraseDeletedEntriesFromFile(char *filename){
	char *fileBuffer;
	struct stat st;
	int fd, fileSize, i, j;
	int verifica = 0;//Verifica se é necessário reescrever o ficheiro.
	//Abre o ficheiro para leitura.
	fd = open(filename, O_RDONLY);
	if (fd < 0){
		printf("open error: %s\n", strerror(errno));
		exit(-1);
	}
	//Obtém o tamanho do ficheiro.
	stat(filename, &st);
	fileSize = st.st_size;
	//Mete o ficheiro em memória volátil.
	fileBuffer = (char *) malloc(sizeof(char)*fileSize);   
	read(fd, fileBuffer, fileSize);

	close(fd);
	//Mete os caracteres dos slots dos KV's eliminados a zeros.
	for(i=0; i<fileSize; i++){
		if(fileBuffer[i] == sentinel){
			verifica = 1;
			j = i + (KEY_SIZE * 2);
			while(i < j){
				fileBuffer[i] = 0;
				i++;
			}
			i--;
		}
	}
	if (verifica > 0){
		//Abre o ficheiro para escrita e elimina o seu conteúdo.
		fd = open(filename, O_TRUNC | O_WRONLY);
		if (fd < 0){
			printf("open error: %s\n", strerror(errno));
			exit(-1);
		}
		//Escreve os slots em memória volátil no ficheiro, à excepçpão dos zeros.
		for(i=0; i<fileSize; i++){
			if(fileBuffer[i]!=0)
				write(fd, &fileBuffer[i], 1);
		}
		close(fd);
	}
	free(fileBuffer);
}

/* Converte um slot numa string */
void slotToString(char *slot){
	int i;
	for(i=0;slot[i]!=' ' && slot[i]!='\n';i++);
	slot[i]='\0';
}

/* Recebe uma string e escreve-a no ficheiro em formato de slot */
void writeSlotToFile(int fd, char *string){
	char *slot;
	int i;
	
	slot = (char *) malloc(sizeof(char) * KEY_SIZE);
	strcpy(slot, string);
	
	for(i=strlen(string); i<KEY_SIZE; i++){
		slot[i] = ' ';
	}
	slot[i-1] = '\n';
	write(fd, slot, KEY_SIZE);//Escreve espaços à frente da string e uma nova linha. (Para poder depois actualizar o slot com novas strings de tamanhos diferentes)

	free(slot);
}

/* Insere o KV lido do ficheiro no shard em memória volátil */
void insertKVfromFileToMemory(int shardId, char *key, char *value){
	int shardPos = hash(key);
	
	if(shards[shardId].KV[shardPos] == NULL){
		shards[shardId].KV[shardPos] = lst_new();
	}
	
	lst_insert(shards[shardId].KV[shardPos], key, value);
}

/* Lê um slot do ficheiro e devolve a string com o '\0' no final */
char *readSlotfromFile(int fd){
	int j;
	char *string;
	string = malloc(sizeof(char) * KEY_SIZE);
	j=0;
	do{
		read(fd, &string[j], 1);
	} while(string[j++]!='\n');
	string[j-1]='\0';
	return string;
}

/* Conta o número de dígitos de um inteiro. Usado para fazer o malloc do nome de um ficheiro com base no número de shards. */
int countDigits(int value)
{
	int result = 0;
	if(value==0) return 1;
	while( value != 0 ) {
		value /= 10;
		result++;
	}
	return result;
}

/* Insere um KV no ficheiro correspondente ao shard. */
void insertKVFile(int shardId, char *key, char *value){
	
	int fd, numOfKV;
	char *filename = malloc(6 + countDigits(numShards) + 1);
	char *string;
	
	sprintf(filename, "fShard%d", shardId);
	//Protege o acesso ao ficheiro.
	pthread_mutex_lock(&fileMutex[shardId]);
	//Open for read and write.
	fd = open(filename, O_RDWR);
	if (fd < 0){
		printf("open error: %s\n", strerror(errno));
		exit(-1);
	}
	//Lê o número de KV's existentes no ficheiro e escreve o valor actualizado (+1).
	string = readSlotfromFile(fd);
	numOfKV = atoi(string);
	numOfKV++;
	sprintf(string, "%d", numOfKV);
	lseek(fd, 0, SEEK_SET);
	writeSlotToFile(fd, string);
	//Verifica se existe algum KV eliminado no ficheiro que possa ser substituído e insere o novo KV.
	if(offsets[shardId]->first == NULL)
		lseek(fd, -1, SEEK_END);
	else{
		lseek(fd, offsets[shardId]->first->offset, SEEK_SET);
	}
	writeSlotToFile(fd, key);
	writeSlotToFile(fd, value);
	if(offsets[shardId]->first == NULL){
		write(fd, &endChar, 1);
	}
	else{
		lst_remove_int(offsets[shardId], offsets[shardId]->first->offset);
	}
	
	free(filename);
	free(string);
	close(fd);
	
	pthread_mutex_unlock(&fileMutex[shardId]);
}

/* Remove um KV do ficheiro */
void removeKVFile(int shardId, char *key){
	
	int i, j, numOfKV, fd, offset;
	char *string, *filename = malloc(6 + countDigits(numShards) + 1);
	char *keySlot = (char *) malloc(sizeof(char) * KEY_SIZE);
	//Mete a chave numa string tipo slot, para depois comparar com cada slot do ficheiro.
	strcpy(keySlot, key);
	for(i=strlen(key); i<KEY_SIZE; i++){
		keySlot[i] = ' ';
	}
	keySlot[i-1] = '\0';
	
	sprintf(filename, "fShard%d", shardId);
	//Protege o acesso ao ficheiro.
	pthread_mutex_lock(&fileMutex[shardId]);
	//Open for read and write.
	fd = open(filename, O_RDWR);
	if (fd < 0){
		printf("open error: %s\n", strerror(errno));
		exit(-1);
	}
	//Lê o número de KV's no ficheiro e actualiza esse valor (-1).
	string = readSlotfromFile(fd);
	numOfKV = atoi(string);
	sprintf(string, "%d", --numOfKV);
	lseek(fd, 0, SEEK_SET);
	writeSlotToFile(fd, string);
	free(string);
	//Inicía a busca pelo KV a remover.
	lseek(fd, 0, SEEK_SET);
	for(i=0; i<(numOfKV+1); i++){
		for(j=0; j<2; j++){
			string = readSlotfromFile(fd);
			if(j==0) free(string);
		}
		if(string[0] == sentinel){//Ignora os que já foram removidos.
			i--;
		}
		else if(! strcmp(string, keySlot)){
			break;
		}
		free(string);
	}
	
	write(fd, &sentinel, 1);//Escreve '<' no primeiro caracter do valor.
	offset = lseek(fd, -strlen(string)-2, SEEK_CUR);
	lst_insert_int(offsets[shardId], offset);//Insere o offset da chave removida na lista de offsets do ficheiro a que corresponde.
	write(fd, &sentinel, 1);//Escreve '<' no primeiro caracter da chave.
	
	free(filename);
	free(string);
	free(keySlot);
	close(fd);
	
	pthread_mutex_unlock(&fileMutex[shardId]);
}

/* Função a correr pelas threads servidoras. */
void *serverThread(void *ptr){
	//int server_id = *( (int*)ptr);
	int i, j, sizeKVArray, shardId, shardPos;
	char *value;
	lst_iitem_t *aux;
	int bufferId;
	
	while(TRUE){
		//Espera que um cliente faça um pedido.
		sem_wait(&serverMayProvide);
		//Actualiza a posição do buffer a que a próxima thread servidora poderá aceder.
		pthread_mutex_lock(&serverMutex);
		bufferId = servptr;
		servptr=(servptr+1) % buffer_size;
		pthread_mutex_unlock(&serverMutex);
		
		delay(); //Deverá ser inserida antes da execução de cada operação (get, put, remove, getAllKeys) pelas tarefas servidoras.
		
		if(! strcmp(buffer[bufferId].command, "kos_get")){
			shardId = buffer[bufferId].shardId;
			shardPos = hash(buffer[bufferId].KV->key);
			
			if(shards[shardId].KV[shardPos] == NULL){
				buffer[bufferId].KV->value[0] = '\0';
			}
			else
			{
				value = lst_search(shards[shardId].KV[shardPos], buffer[bufferId].KV->key);
				if(value == NULL){
					buffer[bufferId].KV->value[0] = '\0';
				}
				else
					strcpy(buffer[bufferId].KV->value, value);
			}
		}
		
		else if(! strcmp(buffer[bufferId].command, "kos_put")){
			shardId = buffer[bufferId].shardId;
			shardPos = hash(buffer[bufferId].KV->key);
			//Protege o acesso à lista do vector do shard.
			pthread_mutex_lock(&shardAndListMutex[shardId + shardPos]);
			
			if(shards[shardId].KV[shardPos] == NULL){
				shards[shardId].KV[shardPos] = lst_new();
				value = NULL;
			}
			else{
				value = lst_search(shards[shardId].KV[shardPos], buffer[bufferId].KV->key);
				
				if(value != NULL){
					lst_remove(shards[shardId].KV[shardPos], buffer[bufferId].KV->key);
					removeKVFile(shardId, buffer[bufferId].KV->key);
				}
			}
			
			lst_insert(shards[shardId].KV[shardPos], buffer[bufferId].KV->key, buffer[bufferId].KV->value);
			
			insertKVFile(shardId, buffer[bufferId].KV->key, buffer[bufferId].KV->value);
			
			if(value == NULL){
				buffer[bufferId].KV->value[0] = '\0';
			}
			else{
				strcpy(buffer[bufferId].KV->value, value);
				free(value);
			}
			
			pthread_mutex_unlock(&shardAndListMutex[shardId + shardPos]);
		}
		
		else if(! strcmp(buffer[bufferId].command, "kos_remove")){
			shardId = buffer[bufferId].shardId;
			shardPos = hash(buffer[bufferId].KV->key);
			//Protege o acesso à lista do vector do shard.
			pthread_mutex_lock(&shardAndListMutex[shardId + shardPos]);
			
			if(shards[shardId].KV[shardPos] == NULL){
				value = NULL;
			}
			else{
				value = lst_search(shards[shardId].KV[shardPos], buffer[bufferId].KV->key);
				lst_remove(shards[shardId].KV[shardPos], buffer[bufferId].KV->key);
			}
			if(value == NULL){
				buffer[bufferId].KV->value[0] = '\0';
			}
			else{
				removeKVFile(shardId, buffer[bufferId].KV->key);
				strcpy(buffer[bufferId].KV->value, value);
				free(value);
			}
			
			pthread_mutex_unlock(&shardAndListMutex[shardId + shardPos]);
		}
		
		
		
		else if(! strcmp(buffer[bufferId].command, "kos_getAllKeys")){
			shardId = buffer[bufferId].shardId;
			//Get size of KV Aray.
			sizeKVArray=0;
			for(i=0; i < HT_SIZE; i++){
				if(shards[shardId].KV[i] != NULL){
					aux = shards[shardId].KV[i]->first;
					while(aux != NULL){
						sizeKVArray++;
						aux = aux->next;
					}
				}
			}
			
			buffer[bufferId].sizeKVArray = sizeKVArray;
			if(sizeKVArray > 0){
				buffer[bufferId].KV = (KV_t *) malloc(sizeof(KV_t)*sizeKVArray);
				//Fill KV Array.
				j=0;
				for(i=0; i < HT_SIZE; i++){
					if(shards[shardId].KV[i] != NULL){
						aux = shards[shardId].KV[i]->first;
						while(aux != NULL){
							strcpy(buffer[bufferId].KV[j].key, aux->KV.key);
							strcpy(buffer[bufferId].KV[j].value, aux->KV.value);
							aux = aux->next;
							j++;
						}
					}
				}
			}
			else
				buffer[bufferId].KV = NULL;
		}
		
		//Diz ao cliente que já pode levantar o pedido.
		sem_post(&buffer[bufferId].bufferSemaphore);
	}
	
	return NULL;
}

/* Inicia as threads servidoras, mecanismos de sincronização, ficheiros... */
int kos_init(int num_server_threads, int buf_size, int num_shards) {
	int i, j;
	
	buffer_size = buf_size;
	numShards = num_shards;

	//Allocate memory for the ID's of the server threads.
	serverIds = (int *) malloc(sizeof(int)*num_server_threads);
	//Allocate memory to hold the pthread_t types.
	threads = (pthread_t *) malloc(sizeof(pthread_t)*num_server_threads);
		
	//Initialize semaphores
	sem_init(&clientMayAsk, 0, buffer_size);
	sem_init(&serverMayProvide, 0, 0);
	
	pthread_mutex_init(&clientMutex, NULL);
	pthread_mutex_init(&serverMutex, NULL);
	/* */
	
	//Initialize Mutex for each list in each shard
	shardAndListMutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t) * num_shards * HT_SIZE);
	for(i=0; i < (num_shards*HT_SIZE); i++)
		pthread_mutex_init(&shardAndListMutex[i], NULL);
		
	fileMutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t) * num_shards);
	for(i=0; i < num_shards; i++)
		pthread_mutex_init(&fileMutex[i], NULL);
	
	//Create Server threads.
	for(i=0; i < num_server_threads; i++){
		serverIds[i] = i;
		
		if (pthread_create(&threads[i], NULL, serverThread, &(serverIds[i])) != 0) {
			printf("Error creating thread.\n");
			free( serverIds );
			free(threads);
			return -1;
		}
	}
	//Initialize the buffer.
	buffer = (buf *) malloc(sizeof(buf)*buf_size);
	bufferMutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t) * buffer_size);
	for(i=0; i < buffer_size; i++){
		sem_init(&buffer[i].bufferSemaphore, 0, 0);
		pthread_mutex_init(&bufferMutex[i], NULL);
	}
	
	//Initialize the shards.
	shards = (shard *) malloc(sizeof(shard)*num_shards);
	for(i=0; i < num_shards; i++){
		shards[i].shardId = i;
		for(j=0; j < HT_SIZE; j++)
			shards[i].KV[j] = NULL;
	}
	
	//Initialize offsets for each file
	offsets = (listInt_t **) malloc(sizeof(listInt_t*)*num_shards);
	for(i=0; i<num_shards; i++){
		offsets[i] = lst_new_int();
	}
	
	//Read from files (or create them)
	char *filename = malloc(6 + countDigits(num_shards) + 1);//fShard+numero+\0
	char *key, *value;
	int fd, numOfKV;
	
	for(i=0; i < num_shards; i++){//Para cada ficheiro
		sprintf(filename, "fShard%d", i);
		fd = open(filename, O_RDONLY);
		if(fd>=0){
			//Lê quantidade de KV's no shard do ficheiro.
			key = readSlotfromFile(fd);
			slotToString(key);
			numOfKV = atoi(key);
			free(key);
			
			//Actualiza o ficheiro removendo os slots de KV's que foram eliminados.
			close(fd);
			eraseDeletedEntriesFromFile(filename);
			fd = open(filename, O_RDONLY);
			if (fd < 0){
				printf("open error: %s\n", strerror(errno));
				exit(-1);
			}
			lseek(fd, KEY_SIZE, SEEK_SET);
			//Lê os KV's e mete-os em memória.
			for(j=0; j<numOfKV; j++){
				key = readSlotfromFile(fd);
				value = readSlotfromFile(fd);
				if(key[0] != sentinel){
					slotToString(key);
					slotToString(value);
					insertKVfromFileToMemory(i, key, value);
				}
				else{
					j--;
				}
				free(key);
				free(value);
			}
		}
		else{
			fd = open(filename, O_CREAT | O_TRUNC | O_WRONLY , S_IRUSR | S_IWUSR);
			if(fd < 0){
				printf("open error: %s\n", strerror(errno));
				return -1;
			}
			writeSlotToFile(fd, "0");
			write(fd, &endChar, 1);
		}
	}
	free(filename);
	
	return 0;

}

char* kos_get(int clientid, int shardId, char* key) {
	char *value=NULL;
	int bufferId;
	
	if(key == NULL)
		return NULL;
	if(clientid < 0 || shardId >= numShards)
		return NULL;

	//Espera que posições do buffer fiquem livres para poder submeter o pedido.
	sem_wait(&clientMayAsk);
	//Actualiza o índice da posição do buffer em que os próximos clientes podem submeter pedidos.
	pthread_mutex_lock(&clientMutex);
	bufferId = cliptr;
	cliptr = (cliptr+1) % buffer_size ;
	
	//Protege o acesso à posição do buffer por outros clientes.
	pthread_mutex_lock(&bufferMutex[bufferId]);
	
	delay();// E após a acquisição de locks para regular o acesso aos dados guardados no KOS.
	
	buffer[bufferId].shardId = shardId;
	buffer[bufferId].KV = (KV_t *) malloc(sizeof(KV_t));
	strcpy(buffer[bufferId].KV->key, key);
	strcpy(buffer[bufferId].command, "kos_get");
	
	pthread_mutex_unlock(&clientMutex);
	
	//Assinala a um servidor que já pode responder a um novo pedido
	sem_post(&serverMayProvide);
	//Espera que o servidor responda ao pedido.
	sem_wait(&buffer[bufferId].bufferSemaphore);
	
	if(buffer[bufferId].KV->value[0] != '\0'){
		value = (char*) malloc(sizeof(char) * strlen(buffer[bufferId].KV->value) + 1);
		strcpy(value, buffer[bufferId].KV->value);
	}
	free( buffer[bufferId].KV );
	buffer[bufferId].KV = NULL;
	buffer[bufferId].command[0] = '\0';

	pthread_mutex_unlock(&bufferMutex[bufferId]);//
	//Permite que seja feito um novo pedido.
	sem_post(&clientMayAsk);//
	
	return value;
}



char* kos_put(int clientid, int shardId, char* key, char* value) {
	char *previousValue=NULL;
	int bufferId;
	
	if(key == NULL || value == NULL)
		return NULL;
	if(clientid < 0 || shardId >= numShards)
		return NULL;
		
	sem_wait(&clientMayAsk);
	
	pthread_mutex_lock(&clientMutex);
	bufferId = cliptr;
	cliptr = (cliptr+1) % buffer_size ;
	
	pthread_mutex_lock(&bufferMutex[bufferId]);
	
	delay();// E após a acquisição de locks para regular o acesso aos dados guardados no KOS.
	
	buffer[bufferId].shardId = shardId;
	buffer[bufferId].KV = (KV_t *) malloc(sizeof(KV_t));
	strcpy(buffer[bufferId].KV->key, key);
	strcpy(buffer[bufferId].KV->value, value);
	strcpy(buffer[bufferId].command, "kos_put");
	
	pthread_mutex_unlock(&clientMutex);
	
	sem_post(&serverMayProvide);
	sem_wait(&buffer[bufferId].bufferSemaphore);
	
	if(buffer[bufferId].KV->value[0] != '\0'){
		previousValue = (char*) malloc(sizeof(char)*strlen(buffer[bufferId].KV->value)+1);
		strcpy(previousValue, buffer[bufferId].KV->value);
	}
	free( buffer[bufferId].KV );
	buffer[bufferId].KV = NULL;
	buffer[bufferId].command[0] = '\0';
	
	pthread_mutex_unlock(&bufferMutex[bufferId]);
	sem_post(&clientMayAsk);
	
	return previousValue;
}

char* kos_remove(int clientid, int shardId, char* key) {
	char *value = NULL;
	int bufferId;
	
	if(key == NULL)
		return NULL;
	if(clientid < 0 || shardId >= numShards)
		return NULL;
		
	sem_wait(&clientMayAsk);
	
	pthread_mutex_lock(&clientMutex);
	bufferId = cliptr;
	cliptr = (cliptr+1) % buffer_size ;
	
	pthread_mutex_lock(&bufferMutex[bufferId]);
	
	delay();// E após a acquisição de locks para regular o acesso aos dados guardados no KOS.
	
	buffer[bufferId].shardId = shardId;
	buffer[bufferId].KV = (KV_t *) malloc(sizeof(KV_t));
	strcpy(buffer[bufferId].KV->key, key);
	strcpy(buffer[bufferId].command, "kos_remove");
	
	pthread_mutex_unlock(&clientMutex);
	
	sem_post(&serverMayProvide);
	sem_wait(&buffer[bufferId].bufferSemaphore);
	
	if(buffer[bufferId].KV->value[0] != '\0'){
		value = (char*) malloc(sizeof(char) * strlen(buffer[bufferId].KV->value) + 1);
		strcpy(value, buffer[bufferId].KV->value);
	}
	free( buffer[bufferId].KV );
	buffer[bufferId].KV = NULL;
	buffer[bufferId].command[0] = '\0';
	
	pthread_mutex_unlock(&bufferMutex[bufferId]);
	sem_post(&clientMayAsk);
	
	return value;
}

KV_t* kos_getAllKeys(int clientid, int shardId, int* dim) {

	KV_t *KV_Array=NULL;
	int bufferId;
	
	if(clientid < 0 || shardId >= numShards){
		*dim = -1;
		return NULL;
	}
	
	sem_wait(&clientMayAsk);
	
	pthread_mutex_lock(&clientMutex);
	bufferId = cliptr;
	cliptr = (cliptr+1) % buffer_size ;
	
	pthread_mutex_lock(&bufferMutex[bufferId]);
	
	delay();// E após a acquisição de locks para regular o acesso aos dados guardados no KOS.
	
	buffer[bufferId].shardId = shardId;
	strcpy(buffer[bufferId].command, "kos_getAllKeys");
	
	pthread_mutex_unlock(&clientMutex);
	
	sem_post(&serverMayProvide);
	sem_wait(&buffer[bufferId].bufferSemaphore);
	
	*dim = buffer[bufferId].sizeKVArray;
	KV_Array = buffer[bufferId].KV;
	buffer[bufferId].KV = NULL;
	buffer[bufferId].command[0] = '\0';
	
	pthread_mutex_unlock(&bufferMutex[bufferId]);
	sem_post(&clientMayAsk);
	
	return KV_Array;
}
