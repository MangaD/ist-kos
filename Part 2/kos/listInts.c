/*
 * list.c - implementation of the Int list functions 
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "listInts.h"



listInt_t* lst_new_int()
{
   listInt_t *list;
   list = (listInt_t*) malloc(sizeof(listInt_t));
   list->first = NULL;
   return list;
}


void lst_insert_int(listInt_t *list, int offset)
{
   lstInt_iitem_t *item, *aux;

   aux=list->first;

   item = (lstInt_iitem_t *) malloc(sizeof(lstInt_iitem_t));
   
   item->offset = offset;
   
   if(aux == NULL){
	  item->next = NULL;
      list->first = item;
  }
   else{
      item->next = list->first;
      list->first = item;
   }
}


int lst_remove_int(listInt_t *list, int offset)
{  
   lstInt_iitem_t *aux, *aux2;
   aux = list->first;
   if(aux == NULL)
      return -1;
   if(aux->offset == offset){
      list->first = aux->next;
      free(aux);
      return 0;
   }
   aux2 = aux;
   aux = aux->next;
   do{
      if(aux != NULL && aux->offset == offset){
         aux2->next = aux->next;
         free(aux);
         return 0;
      }
      else if (aux == NULL) return -1;
      aux2 = aux;
      aux = aux->next;
   } while(aux != NULL);
   return -1;
}
