/*
 * listInts.h - definitions and declarations of the Int list 
 */

#ifndef LISTINT_H
#define LISTINT_H 1

#include <stdlib.h>
#include <stdio.h>

/* lst_iitem - each element of the list points to the next element */
typedef struct lstInt_iitem {
   int offset;
   struct lstInt_iitem *next;
} lstInt_iitem_t;

/* list_t */
typedef struct {
   lstInt_iitem_t * first;
} listInt_t;



/* lst_new - allocates memory for list_t and initializes it */
listInt_t* lst_new_int();

/* lst_insert - insert a new item in the list */
void lst_insert_int(listInt_t *list, int offset);

/* lst_remove - remove first item with 'key' from the list */
int lst_remove_int(listInt_t *list, int offset);//Devolve 0 caso remova, devolve -1 caso não remova.


#endif
