#include <kos_client.h>
#include <stdlib.h>
#include <stdio.h>
//
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include "delay.h"
#include "hash.h"
#include "list.h"

#define TRUE 1

/* Shard Structure */
typedef struct shard {
	int shardId;
	list_t *KV[HT_SIZE];
} shard;
//
/* Buffer Structure */
typedef struct buf{
	char command[20];
	int shardId;
	KV_t *KV;
	int sizeKVArray;
} buf;

//Buffer array
buf *buffer;
//Shard arrays
shard *shards;

int buffer_size;
int numShards;

int *serverIds;
pthread_t *threads;

sem_t *serverSemaphore, *clientSemaphore;

pthread_mutex_t *bufferMutex, *shardAndListMutex;

void *serverThread(void *ptr){
	int server_id = *( (int*)ptr);
	int i, j, sizeKVArray, shardId, shardPos;
	char *value;
	lst_iitem_t *aux;
	
	while(TRUE){
		//Wait for client
		sem_wait(&clientSemaphore[server_id]);
		
		//delay();
		
		
		if(! strcmp(buffer[server_id].command, "kos_get")){
			shardId = buffer[server_id].shardId;
			shardPos = hash(buffer[server_id].KV->key);
			
			if(shards[shardId].KV[shardPos] == NULL){
				buffer[server_id].KV->value[0] = '\0';
			}
			else
			{
				value = lst_search(shards[shardId].KV[shardPos], buffer[server_id].KV->key);
				if(value == NULL){
					buffer[server_id].KV->value[0] = '\0';
				}
				else
					strcpy(buffer[server_id].KV->value, value);
			}
		}
		
		
		
		else if(! strcmp(buffer[server_id].command, "kos_put")){
			shardId = buffer[server_id].shardId;
			shardPos = hash(buffer[server_id].KV->key);
			
			pthread_mutex_lock(&shardAndListMutex[shardId + shardPos]);
			
			if(shards[shardId].KV[shardPos] == NULL){
				shards[shardId].KV[shardPos] = lst_new();
				value = NULL;
			}
			else{
				value = lst_search(shards[shardId].KV[shardPos], buffer[server_id].KV->key);
				
				if(value != NULL)
					lst_remove(shards[shardId].KV[shardPos], buffer[server_id].KV->key);
			}
			
			lst_insert(shards[shardId].KV[shardPos], buffer[server_id].KV->key, buffer[server_id].KV->value);
			
			if(value == NULL){
				buffer[server_id].KV->value[0] = '\0';
			}
			else{
				strcpy(buffer[server_id].KV->value, value);
				free(value);
			}
			pthread_mutex_unlock(&shardAndListMutex[shardId + shardPos]);
		}
		
		
		
		else if(! strcmp(buffer[server_id].command, "kos_remove")){
			shardId = buffer[server_id].shardId;
			shardPos = hash(buffer[server_id].KV->key);
			
			pthread_mutex_lock(&shardAndListMutex[shardId + shardPos]);
			
			if(shards[shardId].KV[shardPos] == NULL){
				value = NULL;
			}
			else{
				value = lst_search(shards[shardId].KV[shardPos], buffer[server_id].KV->key);
				lst_remove(shards[shardId].KV[shardPos], buffer[server_id].KV->key);
			}
			if(value == NULL){
				buffer[server_id].KV->value[0] = '\0';
			}
			else{
				strcpy(buffer[server_id].KV->value, value);
				free(value);
			}
			pthread_mutex_unlock(&shardAndListMutex[shardId + shardPos]);
		}
		
		
		
		else if(! strcmp(buffer[server_id].command, "kos_getAllKeys")){
			shardId = buffer[server_id].shardId;
			//Get size of KV Aray
			sizeKVArray=0;
			for(i=0; i < HT_SIZE; i++){
				if(shards[shardId].KV[i] != NULL){
					aux = shards[shardId].KV[i]->first;
					while(aux != NULL){
						sizeKVArray++;
						aux = aux->next;
					}
				}
			}
			
			buffer[server_id].sizeKVArray = sizeKVArray;
			if(sizeKVArray > 0){
				buffer[server_id].KV = (KV_t *) malloc(sizeof(KV_t)*sizeKVArray);
				//Fill KV Array
				j=0;
				for(i=0; i < HT_SIZE; i++){
					if(shards[shardId].KV[i] != NULL){
						aux = shards[shardId].KV[i]->first;
						while(aux != NULL){
							strcpy(buffer[server_id].KV[j].key, aux->KV.key);
							strcpy(buffer[server_id].KV[j].value, aux->KV.value);
							aux = aux->next;
							j++;
						}
					}
				}
			}
			else
				buffer[server_id].KV = NULL;
		}
		//Post Server
		sem_post(&serverSemaphore[server_id]);
	}
	
	return NULL;
}

int kos_init(int num_server_threads, int buf_size, int num_shards) {
	int i, j;

	//Allocate memory for the ID's of the server threads.
	serverIds = (int *) malloc(sizeof(int)*num_server_threads);
	//Allocate memory to hold the pthread_t types.
	threads = (pthread_t *) malloc(sizeof(pthread_t)*num_server_threads);
	
	//Allocate memory for Server semaphore for each server.
	serverSemaphore = (sem_t *) malloc(sizeof(sem_t) * num_server_threads);
	//Allocate memory for Client semaphore for each client.
	clientSemaphore = (sem_t *) malloc(sizeof(sem_t) * buf_size);
		
	//Initialize Client mutex for each buffer position.
	bufferMutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t) * buf_size);
	for(i=0; i < buf_size; i++)
		pthread_mutex_init(&bufferMutex[i], NULL);
		
	//Initialize Mutex for each list in each shard
	shardAndListMutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t) * num_shards * HT_SIZE);
	for(i=0; i < (num_shards*HT_SIZE); i++)
		pthread_mutex_init(&shardAndListMutex[i], NULL);
	
	
	//Create Server threads.
	for(i=0; i < num_server_threads; i++){
		serverIds[i] = i;
		
		sem_init(&serverSemaphore[i], 0, 0); //Inicializado com 0 (o 2º zero), para se bloquear na 1ª chamada ao wait.
		sem_init(&clientSemaphore[i], 0, 0);
		
		if (pthread_create(&threads[i], NULL, serverThread, &(serverIds[i])) != 0) {
			printf("Error creating thread.\n");
			free( serverIds );
			free(threads);
			return -1;
		}
	}
	//Initialize the buffer.
	buffer_size = buf_size;
	buffer = (buf *) malloc(sizeof(buf)*buf_size);
	
	//Initialize the shards.
	numShards = num_shards;
	shards = (shard *) malloc(sizeof(shard)*num_shards);
	for(i=0; i < num_shards; i++){
		shards[i].shardId = i;
		for(j=0; j < HT_SIZE; j++)
			shards[i].KV[j] = NULL;
	}
	
	
	return 0;

}




char* kos_get(int clientid, int shardId, char* key) {
	char *value=NULL;
	
	if(key == NULL)
		return NULL;
	if(clientid < 0 || shardId >= numShards)
		return NULL;
	if(clientid >= buffer_size)
		clientid %= buffer_size;
	//lock
	pthread_mutex_lock(&bufferMutex[clientid]);
	
	buffer[clientid].shardId = shardId;
	buffer[clientid].KV = (KV_t *) malloc(sizeof(KV_t));
	strcpy(buffer[clientid].KV->key, key);
	strcpy(buffer[clientid].command, "kos_get");

	//Post Client
	sem_post(&clientSemaphore[clientid]);
	//Wait for Server
	sem_wait(&serverSemaphore[clientid]);
	
	if(buffer[clientid].KV->value[0] != '\0'){
		value = (char*) malloc(sizeof(char) * strlen(buffer[clientid].KV->value) + 1);
		strcpy(value, buffer[clientid].KV->value);
	}
	free( buffer[clientid].KV );
	buffer[clientid].KV = NULL;
	buffer[clientid].command[0] = '\0';
	//unlock
	pthread_mutex_unlock(&bufferMutex[clientid]);
	
	return value;
}



char* kos_put(int clientid, int shardId, char* key, char* value) {
	char *previousValue=NULL;
	
	if(key == NULL || value == NULL)
		return NULL;
	if(clientid < 0 || shardId >= numShards)
		return NULL;
	if(clientid >= buffer_size)
		clientid %= buffer_size;
	//lock
	pthread_mutex_lock(&bufferMutex[clientid]);
	
	buffer[clientid].shardId = shardId;
	buffer[clientid].KV = (KV_t *) malloc(sizeof(KV_t));
	strcpy(buffer[clientid].KV->key, key);
	strcpy(buffer[clientid].KV->value, value);
	strcpy(buffer[clientid].command, "kos_put");
	
	//Post Client
	sem_post(&clientSemaphore[clientid]);
	//Wait for Server
	sem_wait(&serverSemaphore[clientid]);
	
	if(buffer[clientid].KV->value[0] != '\0'){
		previousValue = (char*) malloc(sizeof(char)*strlen(buffer[clientid].KV->value)+1);
		strcpy(previousValue, buffer[clientid].KV->value);
	}
	free( buffer[clientid].KV );
	buffer[clientid].KV = NULL;
	buffer[clientid].command[0] = '\0';
	
	//unlock
	pthread_mutex_unlock(&bufferMutex[clientid]);
	
	return previousValue;
}

char* kos_remove(int clientid, int shardId, char* key) {
	char *value = NULL;
	
	if(key == NULL)
		return NULL;
	if(clientid < 0 || shardId >= numShards)
		return NULL;
	if(clientid >= buffer_size)
		clientid %= buffer_size;
		
	//lock
	pthread_mutex_lock(&bufferMutex[clientid]);
	
	buffer[clientid].shardId = shardId;
	buffer[clientid].KV = (KV_t *) malloc(sizeof(KV_t));
	strcpy(buffer[clientid].KV->key, key);
	strcpy(buffer[clientid].command, "kos_remove");
	
	//Post Client
	sem_post(&clientSemaphore[clientid]);
	//Wait for Server
	sem_wait(&serverSemaphore[clientid]);
	
	if(buffer[clientid].KV->value[0] != '\0'){
		value = (char*) malloc(sizeof(char) * strlen(buffer[clientid].KV->value) + 1);
		strcpy(value, buffer[clientid].KV->value);
	}
	free( buffer[clientid].KV );
	buffer[clientid].KV = NULL;
	buffer[clientid].command[0] = '\0';
	//unlock
	pthread_mutex_unlock(&bufferMutex[clientid]);
	
	return value;
}

KV_t* kos_getAllKeys(int clientid, int shardId, int* dim) {

	KV_t *KV_Array=NULL;
	
	if(clientid < 0 || shardId >= numShards){
		*dim = -1;
		return NULL;
	}
	if(clientid >= buffer_size)
		clientid %= buffer_size;
	
	//lock 
	pthread_mutex_lock(&bufferMutex[clientid]);
	
	buffer[clientid].shardId = shardId;
	strcpy(buffer[clientid].command, "kos_getAllKeys");
	
	//Post Client
	sem_post(&clientSemaphore[clientid]);
	//Wait for Server
	sem_wait(&serverSemaphore[clientid]);
	
	*dim = buffer[clientid].sizeKVArray;
	KV_Array = buffer[clientid].KV;
	buffer[clientid].KV = NULL;
	buffer[clientid].command[0] = '\0';
	//unlock 
	pthread_mutex_unlock(&bufferMutex[clientid]);
	
	return KV_Array;
}


