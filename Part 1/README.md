### Part 1

- Synchronization between client and server threads. Same number of client and server threads in which each client thread is associated to a specific server thread.
- Synchronization between server threads, in which access to KOS is protected by a mutex (or semaphore).
- Hashmap used to store KOS data in volatile memory (RAM).

#### Hashmap to store KOS data

The hashmap used to store each partition of KOS is made up of an array of linked lists. The array has a fixed number of elements (specified by the constant `HT_SIZE`). A key-value pair that belongs  to the partition associated with the hashmap is inserted in the list in the `i` position of the array, where `i` is determined using a hash function. This function converts each character of the key to an integer, and sums each of these values into an accumulator variable called `i`. The position of the array of linked lists to access is determined by the result of `i % HT_SIZE`.

#### Buffer for communication between client and server threads

In this first phase it is assumed that the number of client and server threads is equal and is specified by the constant `NUM_THREADS`. The buffer is an array of size `NUM_THREADS`, where each element of the array is associated to a pair `<client thread i, server thread i>`. This means that the requests of a client thread are always processed by the same server thread.

#### Synchronization between server threads for the access to KOS data

In the first part we use a simple mutex/semaphore in order to synchronize the access of the server threads to the KOS data.

Before start processing any request in the buffer, each server thread should execute a call to a function with the signature `void delay()`, defined in the files `include/delay.h` and `kos/delay.c`. This function injects a delay (to simulate, for example, an indeterminism in the operating system), suspending the execution of the thread for a period of time (configured in the file `delay.c`), thus allowing to exercise the mechanisms of synchronization between the server threads.