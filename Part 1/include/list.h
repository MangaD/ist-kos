/*
 * list.h - definitions and declarations of the KV_t list 
 */

#ifndef LIST_H
#define LIST_H 1

#include <stdlib.h>
#include <stdio.h>
#include "kos_client.h"

/* lst_iitem - each element of the list points to the next element */
typedef struct lst_iitem {
   KV_t KV;
   struct lst_iitem *next;
} lst_iitem_t;

/* list_t */
typedef struct {
   lst_iitem_t * first;
} list_t;



/* lst_new - allocates memory for list_t and initializes it */
list_t* lst_new();

/* lst_destroy - free memory of list_t and all its items */
void lst_destroy(list_t *list);

/* lst_insert - insert a new item in the list */
void lst_insert(list_t *list, char *key, char *value);

/* lst_remove - remove first item with 'key' from the list */
int lst_remove(list_t *list, char *key);//Devolve 0 caso remova, devolve -1 caso não remova.

/* lst_print - print the content of list 'list' to standard output */
void lst_print(list_t *list);

/* lst_search - search for the value of a given key */
char *lst_search(list_t *list, char *key);

#endif
