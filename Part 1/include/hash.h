#ifndef HASH_H
#define HASH_H 1

#define HT_SIZE 10

/* O vector tem	um número de elementos fixo	(especificado através da constante HT_SIZE).	
   Um par <chave,valor>	que	pertence à partição	associada à	tabela de dispersão	é inserido	
   na lista na posição i do	vector,	onde i é determinado através de	uma	função dispersão	
  (cujo	código é fornecido no Anexo B).	Esta função	converte cada carácter da chave	num	
  inteiro, e soma cada um destes valores numa variável acumuladora,	chamada	i.	
   A posição do	vector de listas para aceder é determinada como	o resultado	de:	
  i % HT_SIZE.	 
*/
int hash(char* key);

#endif
