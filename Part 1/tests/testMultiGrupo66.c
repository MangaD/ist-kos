#include <kos_client.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_EL 5//
#define NUM_SHARDS 3//
#define NUM_CLIENT_THREADS 3//
#define NUM_SERVER_THREADS 3//
#define BUF_SIZE 3//


//#define DEBUG_PRINT_ENABLED 1  // uncomment to enable DEBUG statements
#if DEBUG_PRINT_ENABLED
#define DEBUG printf
#else
#define DEBUG(format, args...) ((void)0)
#endif


void *client_thread(void *arg) {

	char key[KV_SIZE], value[KV_SIZE];
	char* v;
	int i;
	int client_id=*( (int*)arg);
	//
	KV_t *KV;
	int *sizeKVArray, counter=0;
	//
	//EACH CLIENT USES ONLY 1 SHARD (Corresponding to his ID)
	//Start Inserting
	for (i=NUM_EL; i>=0; i--) {
		sprintf(key, "k-c%d-%d",client_id,i);
		sprintf(value, "val:%d",i);
		v=kos_put(client_id, client_id, key,value);
		DEBUG("C:%d  <%s,%s> inserted in shard %d. Prev Value=%s\n", client_id, key, value, client_id, ( v==NULL ? "<missing>" : v ) );
	}

	printf("------------------- %d:1/2 ENDED INSERTING -----------------------\n",client_id);
	//Ended Inserting
	
	//Start Reading	
	for (i=NUM_EL; i>=0; i--) {
		sprintf(key, "k-c%d-%d",client_id,i);
		sprintf(value, "val:%d",i);
		v=kos_get(client_id, client_id, key);
		if (v==NULL || strncmp(v,value,KV_SIZE)!=0) {
			printf("TEST FAILED --> Error on key %s value should be %s and was returned %s",key,value,v);
			exit(1);
		}
		DEBUG("C:%d  %s %s found in shard %d: value=%s\n", client_id, key, ( v==NULL ? "has not been" : "has been" ),client_id,
								( v==NULL ? "<missing>" : v ) );
		counter++;

	}
	
	printf("------------------ %d:2/2 ENDED READING  ---------------------\n",client_id);
	//Ended Reading
	
	/* START EXTRA */ 
	//
	
	DEBUG("--------------------------------------------------\n");
	sizeKVArray = (int*) malloc(sizeof(int));
	DEBUG("--------------------------\n");
	KV = kos_getAllKeys(client_id, client_id, sizeKVArray);
	//Tests if the size of the array returned by kos_getAllKeys is the same as the number of keys found in the shard
	//in the previous step.
	if (counter != *sizeKVArray) {
		printf("TEST FAILED --> Size of Array and Number of Keys in shard don't match.");
		exit(1);
	}
	for(i=0; i<*sizeKVArray; i++){
		DEBUG("C:%d S:%d    <%s,%s>\n", client_id, client_id, KV[i].key, KV[i].value);
	}
	DEBUG("--------------------------\n");
	free(KV);
	
	DEBUG("--------------------------------------------------\n");
	
	printf("----------------- %d- 3/4 ENDED EXTRA -------------------------\n",client_id);
	//
	/* ENDED EXTRA */
	
	//Start Removing
	for (i=NUM_EL-1; i>=NUM_EL/2; i--) {
		sprintf(key, "k-c%d-%d",client_id,i);
		sprintf(value, "val:%d",i);
		v=kos_remove(client_id, client_id, key);
		if (v==NULL || strncmp(v,value,KV_SIZE)!=0) {
			printf("TEST FAILED --> Error when removing key %s value should be %s and was returned %s",key,value,v);
			exit(1);
		}
		DEBUG("C:%d  %s %s removed from shard %d. value =%s\n", client_id, key, ( v==NULL ? "has not been" : "has been" ),client_id,
								( v==NULL ? "<missing>" : v ) );
	}
	printf("----------------- %d-4/4 ENDED REMOVING -------------------------\n",client_id);
	//Ended Removing

	return NULL;
}


int main(int argc, const  char* argv[] ) {

	int i,s,ret;
	int* res;
	pthread_t* threads=(pthread_t*)malloc(sizeof(pthread_t)*NUM_CLIENT_THREADS);
	int* ids=(int*) malloc(sizeof(int)*NUM_CLIENT_THREADS);

	ret=kos_init(NUM_SERVER_THREADS, BUF_SIZE, NUM_SHARDS);


	if (ret!=0)  {
			printf("kos_init failed with code %d!\n",ret);
			return -1;
		}
		
	for (i=0; i<NUM_CLIENT_THREADS; i++) {	
		ids[i]=i;		
		
		if ( (s=pthread_create(&threads[i], NULL, &client_thread, &(ids[i])) ) ) {
			printf("pthread_create failed with code %d!\n",s);
			return -1;
		}
	}

	for (i=0; i<NUM_CLIENT_THREADS; i++) {	
               s = pthread_join(threads[i], (void**) &res);
               if (s != 0) {
                   printf("pthread_join failed with code %d",s);
			return -1;
		}
           }

	return 0;
}
